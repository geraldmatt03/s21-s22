// [SECTION] Data Modeling

// 1. Identify what information we want to gather from the customers in order to determine whether the user's identity is true.
	// WHY =>
		// 1. TO PROPERLY PLAN out what information will be deemed useful.
		// 2. To lessen the chances or scenarios of having to modify or edit the data stored in the database.
		// 3. To anticipate how this data or information would relate to each other.

// TASK: Create a Couse Booking System for an Institution

	// What are the minimum information would I need to collect from a customer?
	// the following information described below would identify the stucture of the user in our app
	
	// User Documents
		// 1. first Name
		// 2. last Name
		// 3. middle Name
		// 4. email address
		// 5. password/pin
		// 6. mobile number
		// 7. birthdate
		// 8. gender
		// 9. isAdmin: role and restricitions/limitations that this user would have in our app
		// 10. dateTimeRegistered => determines when the student signed up/enrolled in the institutions

	// Course/Subjects
		// 1. Name/Title
		// 2. Course code
		// 3. course description
		// 4. course units
		// 5. course instructor
		// 6. isActive => to describe if the course is being offered by the institution
		// 7. dateTimeCreated => for us to identify when the course is added to the database
		// 8. available slots


	// As a full stack web developer (frontend, backend, database)
	// The more information you gather, the more difficult it is to scale and manage the collection. It is also more difficult to maintain

// 2. Create an ERD to represent the entities inside the database as well as to describe the relationships amongst them.

	// users can have multiple subjects
	// subjects can have multiple enrollees

	// user can have multiple transactions
	// transaction can only belong to a single user

	// a single transaction can have multiple courses
	// a course can be part of multiple transactions

// 3. Convert and translate the ERD into a JSON-like syntax in order to describe the structure of the document inside the collection by creating a MOCK data.

	// NOTE: Creating/using mock data can play an integral role during the testing phase of any app development

	// username: sdsdsdasdasda -> this is one of the things you need to avoid.

	// data => raw and no context -> you want to be able to utilize the storage in your database.

	// you can use this tool to generate mockdata seemlessly: https://www.mockaroo.com/